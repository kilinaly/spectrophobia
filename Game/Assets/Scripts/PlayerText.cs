﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerText : MonoBehaviour
{
    
    [SerializeField] public string currentText;
    //[SerializeField] private GameObject instantiateRoom;
    
    
    [SerializeField] public int nextRoomNumber;
    [SerializeField] public int currentRoomNumber;

    private GameObject gameObjectWithInstantiateRoom;
    private InstantiateRoom instantiateRoom;
    
    private GameObject TextChange;

    private void Start()
    {
        TextChange = GameObject.FindWithTag("TextAbovePlayer");
        gameObjectWithInstantiateRoom =  GameObject.FindWithTag("InstantiateRoom");
        instantiateRoom = gameObjectWithInstantiateRoom.GetComponent<InstantiateRoom>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            TextChange.GetComponent<Text>().text = currentText;
        if ((nextRoomNumber!=10)&&(instantiateRoom.canBeTriggered==true))//чтобы рука срабатывала только на дверях и лестницах и ничего не мешало появлению персонажа
        {
            instantiateRoom.doorTeg = this.tag;
            instantiateRoom.handCanBeUsed = true;
        }
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            TextChange.GetComponent<Text>().text = null;
            if ((nextRoomNumber != 10) && (instantiateRoom.canBeTriggered == true))
            {
                instantiateRoom.doorTeg = null;
                instantiateRoom.handCanBeUsed = false;
            }
        }
    }
}
