﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    private GameObject player;
    public GameObject text;
    public bool shouldMove = true;
    
    void Start()
    {
        player = GameObject.FindWithTag("Player");

    }
    
    void Update()
    {
        if (shouldMove==true)
        {
            this.transform.position = new Vector3(player.transform.position.x, player.transform.position.y + 2.4f, -10f);  
        }

        text.transform.position = new Vector3(player.transform.position.x, text.transform.position.y, text.transform.position.z); 
    }
}
