﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
    public class TextAbovePlayer
    {
        //employees is case sensitive and must match the string "employees" in the JSON.
        public OnlyOneTextAbovePlayer[] textAbovePlayer;
    }

[System.Serializable]
public class OnlyOneTextAbovePlayer
{
    //these variables are case sensitive and must match the strings "firstName" and "lastName" in the JSON.
    public string key;
    public string value;
}