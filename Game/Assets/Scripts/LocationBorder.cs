﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocationBorder : MonoBehaviour
{
    [SerializeField] public int leftRight;
    private GameObject camera;
    private CameraMovement cameraMovement;
    private GameObject player;
    public bool canTrigger;
    
    public GameObject gLocationBorder1;
    private LocationBorder1 locationBorder1;

    private void Start()
    {
        canTrigger = true;
        camera = GameObject.FindWithTag("MainCamera");
        cameraMovement = camera.GetComponent<CameraMovement>();
        player = GameObject.FindWithTag("Player");
        locationBorder1 = gLocationBorder1.GetComponent<LocationBorder1>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "RightBorder")
        {
            cameraMovement.shouldMove = false;
            locationBorder1.canTrigger = false;
        }
    }

    private void Update()
    {
        if ((leftRight == 2)&&(canTrigger==true))
        {
            if ((cameraMovement.shouldMove == false) && (player.transform.position.x < camera.transform.position.x))
            {
                cameraMovement.shouldMove = true;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "RightBorder")
        {
            cameraMovement.shouldMove = true;
            locationBorder1.canTrigger = true;
        }
    }
}
