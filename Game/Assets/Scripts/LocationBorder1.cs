﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocationBorder1 : MonoBehaviour
{
    [SerializeField] public int leftRight;
    private GameObject camera;
    private CameraMovement cameraMovement;
    private GameObject player;
    public bool canTrigger;
    
    public GameObject gLocationBorder;
    private LocationBorder locationBorder;

    private void Start()
    {
        canTrigger = true;
        camera = GameObject.FindWithTag("MainCamera");
        cameraMovement = camera.GetComponent<CameraMovement>();
        player = GameObject.FindWithTag("Player");
        locationBorder = gLocationBorder.GetComponent<LocationBorder>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "LeftBorder")
        {
            cameraMovement.shouldMove = false;
            locationBorder.canTrigger = false;
        }
    }

    private void Update()
    {
        if ((leftRight == 1)&&(canTrigger==true))
        {
            if ((cameraMovement.shouldMove == false) && (player.transform.position.x > camera.transform.position.x))
            {
                cameraMovement.shouldMove = true;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "LeftBorder")
        {
            cameraMovement.shouldMove = true;
            locationBorder.canTrigger = true;
        }
    }
}
