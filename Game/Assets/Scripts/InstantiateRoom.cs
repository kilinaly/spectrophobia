﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiateRoom : MonoBehaviour
{
    [SerializeField] public GameObject[] rooms;
    public string doorTeg;
    public bool handCanBeUsed = false;
    public bool canBeTriggered = true;

    public void InstantiateNewRoom()
    {
        canBeTriggered = false;
        
        GameObject door =  GameObject.FindWithTag(doorTeg);
        PlayerText playerText = door.GetComponent<PlayerText>();

        if (handCanBeUsed==true)
        {
            //уничтожение текущей локации
            GameObject currentRoom =  GameObject.FindWithTag("CurrentRoom");
            Destroy(currentRoom);
            
            
            
            //создание следующей локации
            GameObject newCurrentRoom = Instantiate(rooms[playerText.nextRoomNumber], new Vector3(0, 0, 0), Quaternion.identity);
            newCurrentRoom.tag = "CurrentRoom";

            GameObject player = GameObject.FindWithTag("Player");
            float playerPosY = player.transform.position.y;
            switch (doorTeg)//для корректного перехода персонажа в комнату
            {
                case "BedroomCorridor": doorTeg = "CorridorBedroom";
                    playerPosY =-4.085f;
                    break;
                case "BedroomBathroom": doorTeg = "BathroomBedroom";
                    playerPosY= - 4.025f;
                    break;
                case "CastleAfterEntering": doorTeg = "AfterEnteringCastle";
                    playerPosY = -3.695f;
                    break;
                case "AfterEnteringCastle": doorTeg = "CastleAfterEntering";
                    playerPosY = -2.705f;
                    break;
                case "MainKitchen": doorTeg = "KitchenMain";
                    playerPosY = -4.065001f;
                    break;
                case "KitchenMain": doorTeg = "MainKitchen";
                    playerPosY =-3.695001f;
                    break;
                case "CabinetCorridor": doorTeg = "CorridorCabinet";
                    playerPosY = -4.085f;
                    break;
                case "AfterEnteringCorridor": doorTeg = "CorridorMain";
                    playerPosY = -4.085f;
                    break;
                case "BathroomBedroom": doorTeg = "BedroomBathroom";
                    playerPosY = -3.805f;
                    break;
                case "3floorCabinet": doorTeg = "Cabinet3floor";
                    playerPosY = -4.015f;
                    break;
                case "Cabinet3floor": doorTeg = "3floorCabinet";
                    playerPosY = -4.27f;
                    break;
                case "CorridorBedroom": doorTeg = "BedroomCorridor";
                    playerPosY = -3.805f;
                    break;
                case "CorridorCabinet": doorTeg = "CabinetCorridor";
                    playerPosY = -4.015f;
                    break;
                case "CorridorMain": doorTeg = "AfterEnteringCorridor";
                    playerPosY = -3.695001f;
                    break;
                case "Gate": doorTeg = "CastleAfterEntering";
                    playerPosY = -2.705f;
                    break;
                
            }
            
            GameObject newDoor =  GameObject.FindWithTag(doorTeg);
            
            StartCoroutine("FadeIn");
            
            player.transform.position =  new Vector3(newDoor.transform.position.x, playerPosY, player.transform.position.z);
        }
        
    }

    IEnumerator FadeIn()
    {
        for (float f = 0.02f; f <=1; f+=0.02f)
        {
            GameObject player = GameObject.FindWithTag("PlayerGraphics");
            SpriteRenderer spriteRenderer = player.GetComponent<SpriteRenderer>();
            Color c = spriteRenderer.material.color;
            c.a = f;
            spriteRenderer.material.color = c;
            if (f > 0.8)
            {
                canBeTriggered = true;
            }
            yield return new WaitForSeconds(0.02f);
        }
    }
    IEnumerator FadeOut()
    {
        for (float f = 1f; f >=-0.02; f-=0.02f)
        {
            GameObject player = GameObject.FindWithTag("PlayerGraphics");
            SpriteRenderer spriteRenderer = player.GetComponent<SpriteRenderer>();
            Color c = spriteRenderer.material.color;
            c.a = f;
            spriteRenderer.material.color = c;
            yield return new WaitForSeconds(0.02f);
        }
    }
    
}
